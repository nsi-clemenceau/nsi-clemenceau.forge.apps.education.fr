---
author: David Landry
title: 👏 Crédits
---

Le site est hébergé par  [la forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/){:target="_blank" }.

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Un grand merci à M. Vincent-Xavier Jumel et Vincent Bouillot qui ont réalisé la partie technique de ce site. Merci également à Mireille Coilhac, c'est à partir de son site que celui-ci est construit.

Le logo : material-wizard-hat: fait partie de mkdocs sous la référence wizard-hat
