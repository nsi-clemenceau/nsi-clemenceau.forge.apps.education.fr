---
author: David Landry
title: 🏡 Accueil
---

# Bienvenue sur la page web NSI du Lycée Clemenceau à Nantes

Vous y trouverez...

[Un dépôt sur la forge des communs numériques](https://forge.apps.education.fr/nsi-clemenceau/nsi-1ere)

[Un site web pour la NSI en terminale](https://nsi-clemenceau.forge.apps.education.fr/nsi-terminale/)

